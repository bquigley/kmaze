import pygame
import sys
import random
import logging
import math

from util import *
import colors

DEBUG = True
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

if DEBUG:
    import tests

class Ball:

    def __init__(self, parent):
        assert type(parent) == MazeApp
        self.parent = parent
        self._pos = [int(self.parent.worldx / 2.0), 20.0]
        self._pos[1] = self.parent.maze.pos[1] - self.parent.maze.rings[1] - 7
        self._vel = [0.0, 0.0]
        self.color = colors.green
        self.radius = 5
        self._ghost_ticks = 0 # Support a few seconds of intangibility to escape bouncing trap

    @property
    def pos(self):
        return (int(self._pos[0]), int(self._pos[1]))

    def __str__(self):
        return "the ball"

    def _fall(self):
        self._vel[1] += 0.02

    def _update(self):
        self._pos[0] += self._vel[0]
        self._pos[1] += self._vel[1]
        if self._ghost_ticks:
            self._ghost_ticks -= 1

    def draw(self):
        self._update()
        self._fall()
        pygame.draw.circle(
                self.parent.canvas, self.color, self.pos, self.radius
            )

    def bounce_off(self, wall_angle, friction=.85):
        """
        Update the object's velocity to bounce it off a surface at an angle.
        :param reflection_angle (radians from "north"): For a flat horizontal surface like a floor, this would be 0.
        :param friction: A friction coefficient.
        """
        if self._ghost_ticks:
            logger.info("Avoided a corner trap with {} ghost ticks.".format(self._ghost_ticks))
            return 0
        velocity, original_angle = line_and_angle_from(*self._vel)
        new_angle = reflect(original_angle, wall_angle)
        new_velocity = velocity * friction
        dx, dy = dx_dy_from(new_velocity, new_angle)
        self._vel = [dx, dy]
        self._ghost_ticks = 2
        return 0

class Location:
    """
    An object in the maze such as a wall or the objective.
    """
    def __init__(self, parent, ring, rotation, offset=0):
        # Each location has a ring to emerge out from, and a radian location.
        assert type(parent) == MazeApp
        assert type(offset) == int
        self.parent = parent
        self.ring = ring
        self.offset = offset
        self._rotation = rotation

    @property
    def __str__(self):
        return "the {}'s location".format(self.parent)

    @property
    def rotation(self):
        """
        The point's location on the maze in radians,
        taking into account how the maze might have rotated.
        """
        return self._rotation + self.parent.maze.radian_rotation

    @property
    def point(self):
        # Each coordinate is the center of the circle,
        # plus a rise and a run created using trigonometry and the radian measurement.
        return (int(self.parent.maze.pos[0] + (self.ring + self.offset) * math.sin(self.rotation * math.pi)),
                int(self.parent.maze.pos[1] + (self.ring + self.offset) * math.cos(self.rotation * math.pi)))

class Wall():

    def __init__(self, parent, ring, rotation):
        assert type(parent) == Maze
        self.parent = parent
        self.ring = ring
        self.color = parent.color
        self.bottom = Location(parent.parent, ring, rotation, offset=0)
        self.top = Location(parent.parent, ring, rotation, offset=15)

    def __str__(self):
        return "a wall in ring # {}".format(self.ring)

    def draw(self):
        """
        Draw the wall, using its distance from the center, and
        number of pi-radians rotated.
        Its starting location is the center plus a rise and a run -
        the rise is 100% of the tier length if the radian_rotation is 0.
        Its length is 15.
        """
        pygame.draw.line(self.parent.parent.canvas, self.parent.color, self.top.point, self.bottom.point, 2)


class Marker:
    """
    An object in the maze represented as a circle.
    """
    def __init__(self, name, parent, ring, rotation, color, glowing=0):
        assert type(parent) == MazeApp
        assert glowing in [-1, 0, 1]
        self.name = name
        self.parent = parent
        self.center = parent.maze.pos
        self.ring = ring
        self.natural_color = color
        self.color = color[:]
        self.location = Location(self.parent, ring, rotation, offset=7)
        self.glowing = glowing
        self.glowing_target_color = colors.white

    @property
    def __str__(self):
        return self.name

    def _update_color(self):
        assert self.glowing != 0
        if self.color == colors.white:
            self.glowing_target_color = self.natural_color
        elif self.color == self.natural_color:
            self.glowing_target_color = colors.white
        for i, cur_rgb_value in enumerate(self.color):
            target_rgb_value = self.glowing_target_color[i]
            if cur_rgb_value != target_rgb_value:
                difference = target_rgb_value - cur_rgb_value
                direction_unit = difference/abs(difference)
                assert abs(direction_unit) == 1
                self.color[i] += direction_unit

    def draw(self):
        if self.glowing:
            self._update_color()
        pygame.draw.circle(self.parent.canvas, self.color, self.location.point, 4, 0)

class Maze:
    def __init__(self, parent, radius=None):
        assert type(parent) == MazeApp
        self.parent = parent
        self.radius = radius or int(min(self.parent.worldx, self.parent.worldy)/2)-50
        self.circumference = 2*math.pi*self.radius
        self.pos = (
                    int(self.parent.worldx/2),
                    int(self.parent.worldy/2),
                   )
        self.color = colors.lilac  # todo
        """
        Measurements in radians, counting clockwise starting from top
        """
        self.radian_rotation = 0
        self._radian_rotation_vel = 0

        self.spinnability = .005
        self.friction = .9912

        self.entrance = 0
        self.exit = random.random() * 2 # Bottom
        self.rings = range(self.radius, 0, -15)
        self.walls = []
       # Create 30 walls at random locations.
        for _ in range(40):
            ring = random.choice(self.rings[1:]) # Choose a random ring, except the outermost one.
            radian_rotation = random.random()*2*math.pi
            wall = Wall(self, ring, radian_rotation)
            self.walls.append(wall)

    def _collide_rings(self, obj):
        """
        Checks an object in space with a pos property to see if it has
        run into a ring and/or a wall. If so, send it a bounce_off.
        :param obj: A circular object (the ball).
        """
        obj_distance_from_center = distance(self.pos, obj.pos)
        # Check if the object is colliding with a ring.
        closest_ring = closest(self.rings, obj_distance_from_center)
        obj_distance_from_ring = obj_distance_from_center - closest_ring
        if abs(obj_distance_from_ring) <= obj.radius:
            angle = .5+math.tan((obj.pos[0]-self.pos[0])/(obj.pos[1]-self.pos[1]))
            logger.info("Bouncing off of a {} angle.".format(angle))
            obj.bounce_off(angle)

        # Check if it's colliding with a wall.
        # Find the closest inner ring, in order to check its walls.
        inner_ring = None
        for ring in self.rings:
            if ring > obj_distance_from_center:
                break
            inner_ring = ring
        # Select the walls on that closest inner ring.
        nearby_walls = [wall for wall in self.walls if wall.ring == inner_ring]
        # for wall in nearby_walls:
        # ^- Todo: This line isn't working.
        for wall in self.walls:
            wall_vector = pygame.math.Vector2(obj.pos, wall.bottom.point)
            wall_distance = wall_vector.distance_to(wall.top.point)
            print("Distance:", wall_distance)
            if wall_distance <= obj.radius:
            # ^- Todo: this line isn't working
                logger.info("Bouncing off a wall ({})".format(wall))
                bounce_angle = wall_vector.angle_to(obj.pos)
                obj.bounce_off()

    # Calculate the final direction
    # Bounce it off

    def _update(self):
        self.radian_rotation += self._radian_rotation_vel
        self.friction_slow()

    def rotate(self, direction, rate=None):
        """
        Rotate the maze.
        :param direction: 1 for clockwise motion, -1 for counterclockwise motion>
        :param rate: The speed of motion.
        """
        rate = rate or self.spinnability
        self._radian_rotation_vel += rate * direction

    def friction_slow(self):
        """
        Slow the rotation of the maze a little due to friction.
        """
        self._radian_rotation_vel *= self.friction

    def draw(self):
        self._collide_rings(self.parent.ball)
        self._update()
        for ring in self.rings:
            # Draw a lilac maze wall:
            pygame.draw.circle(
                self.parent.canvas, self.color, self.pos, ring, 2
            )
        for wall in self.walls:
            wall.draw()


class Solution:
    def __init__(self, parent):
        self.parent = parent
        # Directions format: "in", "out", or an integer angle in radians (+ or -)
        # todo
        self.directions = []

    def __str__(self):
        return repr(self.directions)

    def show(self):
        # Light up the way through.
        # todo
        pass


class MazeApp:

    def __init__(self):
        """
        Declare basic game parameters.
        """
        self.playing = True
        self.worldx = 960
        self.worldy = 720
        self.dims = (
            0,            # x
            0,            # y
            self.worldx,  # width
            self.worldy,  # height
        )
        self.fps = 40  # Frames per second
        self.ani = 40  # Animation cycles
        self.clock = pygame.time.Clock()
        pygame.init()
        self.canvas = pygame.display.set_mode([self.worldx, self.worldy])
        self.maze = Maze(self)
        self.ball = Ball(self)
        self.goal = Marker("objective", self, self.maze.rings[1], 0, colors.purple, glowing=1)
        self.solution = Solution(self)
        self.objects = [self.maze, self.ball, self.goal]

    def __str__(self):
        return "the application"

    def run(self):
        """
        Run the main game loop.
        """
        while self.playing:
           self.draw()
           self.clock.tick(self.fps)
           for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self.exit()
                elif event.type == pygame.KEYDOWN:
                    if DEBUG and chr(event.key) in ['q']:
                        # Need to build a proper menu with Exit when done debugging.
                        self.exit()
                    elif chr(event.key) in ['l', 'ē']:
                        # rotate clockwise
                        self.maze.rotate(1)
                    elif chr(event.key) in ['j', 'Ĕ']:
                        # rotate counterclockwise
                        self.maze.rotate(-1)
                        # TODO Add real keybinds in Options menu
                    else:
                        logger.info("{} key was pressed (ord {})".format(chr(event.key), event.key))
           pygame.display.flip()

    def draw(self):
        self.canvas.fill(colors.gray)
        for obj in self.objects:
            obj.draw()

    def exit(self):
        self.playing = False
        pygame.quit()
        sys.exit()

def main():
    m = MazeApp()
    m.run()

if __name__ == "__main__":
    main()

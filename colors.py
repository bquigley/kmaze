from util import rgb

white  = rgb('#EDFFEC')
green  = rgb('#61E786')
purple = rgb('#48435C')
lilac  = rgb('#9792E3')
gray   = rgb('#1C1B1D')
dark   = rgb('#0E0D11')

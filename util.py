import pygame
import math

"""
Geometric & other calculation tools.
"""

def distance(point_A, point_B):
    return math.sqrt((point_A[0] - point_B[0])**2+(point_A[1] - point_B[1])**2)


def distance_to_line(point_A, line_endpoint_A, line_endpoint_B):
    """
    Return the distance between a point and a line constituting two points.
    """
    line = pygame.math.Vector2(point_A, line_endpoint_A)
    return line.distance_to(line_endpoint_B)


def closest(li, num):
    """
    Returns closest value from list to num.

    If two numbers are equally close, return the smallest number.
    """
    return min(li, key=lambda x: abs(x - num))


"""
Let all angles be measured starting from "north."
"""

def degrees_from(radians):
    # Shouldn't need this.
    return (radians*180) % 360


def radians_from(degrees):
    # Shouldn't need this.
    return (degrees/180) % 2


def line_and_angle_from(dx, dy):
    line = math.sqrt(dx**2+dy**2)
    try:
        angle = math.atan(dy/dx)/math.pi
    except ZeroDivisionError:
        if dy > 0:
            angle = .5
        elif dy < 0:
            angle = -.5
        elif dx == 0 and dy == 0:
            logger.info("Called line_and_angle_from(dx, dy) with 0, 0 params.")
            angle = None
    angle += .5  # Add the angle to my reference point of north
    angle = angle % 2
    return line, angle


def dx_dy_from(line, angle):
    angle -= .5  # Get the angle from my reference point of north
    dx = math.cos(angle*math.pi) * line
    dy = math.sin(angle*math.pi) * line
    return dx, dy


def reflect(original_angle, wall_angle):
    """
    Calculate the angle of reflection after bouncing an angle off a wall.
    """
    angle_of_reflection = wall_angle - original_angle
    return (original_angle+angle_of_reflection*2) % 2


def rgb(hex_code):
    """
    Convert from hex_code codes to rgb.
    """
    if hex_code[0] == "#":
        hex_code = hex_code[1:]
    return [int(hex_code[i:i+2], 16) for i in (0, 2, 4)]

from util import *

# Tests
assert distance([0, 0], [3, 4]) == 5
assert closest([2, 3, 4], 3.6) == 4
assert closest([2, 5, 8], 3.6) == 5
assert degrees_from(1) == 180
assert degrees_from(2) == 0
assert degrees_from(1.5) == 270
assert degrees_from(-.5) == 270
assert radians_from(270) == 1.5
assert radians_from(90) == .5
assert line_and_angle_from(0, 1) == (1, 1)
assert line_and_angle_from(1, 0) == (1, .5)
assert line_and_angle_from(-3, 4)[0] == 5
assert line_and_angle_from(80, -80)[1] == .25
assert reflect(0, 0) == 0                                               # No change
assert reflect(radians_from(10), 0) == radians_from(-10 % 360)
assert reflect(radians_from(45), 0) == radians_from(-45 % 360)
assert reflect(radians_from(90), 0) == radians_from(270)                # 180 reversal
assert reflect(radians_from(135), 0) == radians_from(225)
assert reflect(radians_from(180), 0) == radians_from(180)               # No change
assert reflect(radians_from(225), 0) == radians_from(135)
assert reflect(radians_from(270), 0) == radians_from(90)                # 180 reversal
assert reflect(radians_from(315), 0) == radians_from(45)
assert reflect(radians_from(360), 0) == 0                               # No change
assert reflect(0, radians_from(90)) == radians_from(180)                 # 180 reversal
assert reflect(radians_from(10), radians_from(90)) == radians_from(170)  # 80 degree difference off wall angle?
assert reflect(radians_from(45), radians_from(90)) == radians_from(135)
assert reflect(radians_from(90), radians_from(90)) == radians_from(90)   # No change
assert reflect(radians_from(135), radians_from(90)) == radians_from(45)
assert reflect(radians_from(180), radians_from(90)) == 0                 # 180 reversal
assert distance_to_line([0, 0], [2, 2], [2, 0]) == 2


"""
This block of tests doesn't work because of floating point imprecision;
however I'm leaving it here for troubleshooting if needed.

logger.info("Expected dx_dy_from(5, .75) == (3, 4); got {}".format(dx_dy_from(5, .75)))
assert dx_dy_from(1, 0) == (0, -1)
assert dx_dy_from(3, 0) == (0, -3)
assert dx_dy_from(5, .75) == (3, 4)
"""

print("All tests passed OK.")

Kaleidomaze
==========

Here is a game about playing through a beautiful maze!

TODO
----

* Add openings
* Add steering (and remove spinning controls)
* Add collision detection for gates
* And for the game edges
* Add non-collision for doors
* Move from requirements.txt to setup.py
* Build executable

Bugs
----

